﻿namespace MyTunes.Models
{
    public class TopGenre
    {
        public string Genre { get; set; }
        public int Count { get; set; }
    }
}