﻿using MyTunes.Models.Domain;
using MyTunes.Models.DTOs;

namespace MyTunes.Models
{
    public class HighestSpender
    {
        public CustomerDTO Customer { get; set; }
        public decimal Total { get; set; }
    }
}