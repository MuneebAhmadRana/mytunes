﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Models.DTOs
{
    public class CustomerDTO
    {
        public int CustomerId { get; set; }

        [StringLength(40, ErrorMessage = "First Name cannot be longer than 40")]
        public string FirstName { get; set; }

        [StringLength(20, ErrorMessage = "Last Name cannot be longer than 40")]
        public string LastName { get; set; }


        [Required]
        [StringLength(40, ErrorMessage = "Country cannot be longer than 40")]
        public string Country { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "Postal Code cannot be longer than 40")]
        public string PostalCode { get; set; }

        [Required]
        [StringLength(24, ErrorMessage = "Phone length cannot be longer than 40")]
        public string Phone { get; set; }


        [StringLength(60, ErrorMessage = "Email address cannot be longer than 40")]
        public string Email { get; set; }

    }
}
