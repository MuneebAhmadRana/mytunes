namespace MyTunes.Models.Domain
{
    public class TrackInfo
    {
        public string TrackName { get; set; }
        public string ArtistName { get; set; }
        public string AlbumTitle { get; set; }
        public string GenreName { get; set; }
    }
}