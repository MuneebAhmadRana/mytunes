﻿using MyTunes.Models.Domain;
using System.Collections.Generic;

namespace MyTunes.Models
{
    public class Random5
    {
        public Random5()
        {
            TrackList = new List<Track>();
            GenreList = new List<Genre>();
            ArtistList = new List<Artist>();
        }

        public List<Track> TrackList { get; set; }
        public List<Genre> GenreList { get; set; }
        public List<Artist> ArtistList { get; set; }
    }
}