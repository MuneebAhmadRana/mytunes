﻿namespace MyTunes.Models
{
    public class CountryCount
    {
        public string Country { get; set; }
        public int Count { get; set; }
    }
}