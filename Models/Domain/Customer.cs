﻿using System.ComponentModel.DataAnnotations;

namespace MyTunes.Models.Domain
{
    public class Customer
    {
        public int CustomerId { get; set; }

        [StringLength(40, ErrorMessage = "First Name cannot be longer than 40")]
        public string FirstName { get; set; }

        [StringLength(20, ErrorMessage = "Last Name cannot be longer than 40")]
        public string LastName { get; set; }

        [Required]
        [StringLength(80, ErrorMessage = "Company Name cannot be longer than 80")]
        public string Company { get; set; }

        [Required]
        [StringLength(70, ErrorMessage = "Address cannot be longer than 70")]
        public string Address { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "City cannot be longer than 40")]
        public string City { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "State cannot be longer than 40")]
        public string State { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "Country cannot be longer than 40")]
        public string Country { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "Postal Code cannot be longer than 40")]
        public string PostalCode { get; set; }

        [Required]
        [StringLength(24, ErrorMessage = "Phone length cannot be longer than 40")]
        public string Phone { get; set; }

        [Required]
        [StringLength(24, ErrorMessage = "Fax length cannot be longer than 40")]
        public string Fax { get; set; }

        [StringLength(60, ErrorMessage = "Email address cannot be longer than 40")]
        public string Email { get; set; }

        [Required]
        public int SupportRepId { get; set; }
    }
}