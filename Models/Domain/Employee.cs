﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MyTunes.Models.Domain
{
    public class Employee
    {
        public int EmployeeId { get; set; }

        [StringLength(20, ErrorMessage = "Last Name cannot be longer than 20")]
        public string LastName { get; set; }

        [StringLength(20, ErrorMessage = "First Name cannot be longer than 20")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Title cannot be longer than 30")]
        public string Title { get; set; }

        [Required]
        public int ReportsTo { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        [Required]
        public DateTime HireDate { get; set; }

        [Required]
        [StringLength(70, ErrorMessage = "Address cannot be longer than 70")]
        public string Address { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "City cannot be longer than 40")]
        public string City { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "State cannot be longer than 40")]
        public string State { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "Country cannot be longer than 40")]
        public string Country { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "Postal Code cannot be longer than 10")]
        public string PostalCode { get; set; }

        [Required]
        [StringLength(24, ErrorMessage = "Phone cannot be longer than 24")]
        public string Phone { get; set; }

        [Required]
        [StringLength(24, ErrorMessage = "Fax cannot be longer than 24")]
        public string Fax { get; set; }

        [Required]
        [StringLength(60, ErrorMessage = "Email cannot be longer than 60")]
        public string Email { get; set; }
    }
}