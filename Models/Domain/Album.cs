using System.ComponentModel.DataAnnotations;

namespace MyTunes.Models.Domain
{
    public class Album
    {
        [Required]
        [StringLength(160)]
        public int AlbumId { get; set; }

        [Required]
        [StringLength(160)]
        public string Title { get; set; }

        [Required]
        public int ArtistId { get; set; }
    }
}