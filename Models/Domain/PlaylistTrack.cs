using System.ComponentModel.DataAnnotations;

namespace MyTunes.Models.Domain
{
    public class PlaylistTrack
    {
        [Required]
        public int PlaylistId { get; set; }

        [Required]
        public int TrackId { get; set; }
    }
}