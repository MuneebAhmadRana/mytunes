﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MyTunes.Models.Domain
{
    public class Invoice
    {
        public int InvoiceId { get; set; }
        public int CustomerId { get; set; }
        public DateTime InvoiceDate { get; set; }

        [Required]
        [StringLength(70, ErrorMessage = "Billing Address cannot be longer than 70")]
        public string BillingAddress { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "Billing City cannot be longer than 40")]
        public string BillingCity { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "Billing State cannot be longer than 40")]
        public string BillingState { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "Billing Country cannot be longer than 40")]
        public string BillingCountry { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "Billing PostalCode cannot be longer than 10")]
        public string BillingPostalCode { get; set; }

        [Range(0, 99999999.99)]
        public decimal Total { get; set; }
    }
}