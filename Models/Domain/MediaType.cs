using System.ComponentModel.DataAnnotations;

namespace MyTunes.Models.Domain
{
    public class MediaType
    {
        [Required]
        public int MediaTypeId { get; set; }

        [StringLength(120)]
        public string Name { get; set; }
    }
}