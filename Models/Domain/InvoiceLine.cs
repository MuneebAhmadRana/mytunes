﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MyTunes.Models.Domain
{
    public class InvoiceLine
    {
        public int InvoiceLineId { get; set; }
        public int InvoiceId { get; set; }
        public int TrackId { get; set; }

        [Range(0, 99999999.99)]
        public decimal UnitPrice { get; set; }

        public int Quantity { get; set; }
    }
}