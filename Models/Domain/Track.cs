using System.ComponentModel.DataAnnotations;

namespace MyTunes.Models.Domain
{
    public class Track
    {
        [Required]
        public int TrackId { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        public int AlbumId { get; set; }

        [Required]
        public int MediTypeId { get; set; }

        public int GenreId { get; set; }

        [StringLength(220)]
        public string Composer { get; set; }

        [Required]
        public int Milliseconds { get; set; }

        public int Bytes { get; set; }

        [Required]
        [Range(0, 99999999.99)]
        public decimal UnitPrice { get; set; }
    }
}