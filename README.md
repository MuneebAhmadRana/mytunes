# MyTunes
#### Created by Andreas Vu and Muneeb Rana

## Features
The API is divided into two sections. The base api route is **/api/v1**
1. MusicController
2. CustomerController

### Music Controller
The music controller is incharge of all music endpoints. The endpoints are as follows:

- GET /music -> Fetches 5 random Artists, Tracks and Genres
- GET /music/search?track={trackName} -> Fetches information about the specified track.

### CustomerController
The customer controller is incharge of all the customer endpoints. The endpoints are as follows:

- GET /customers -> Fetches all customers from the database
- GET /customers/{id} -> Fetches a customer based on the provided Id
- POST /customers -> Adds a new customer to the database
- PUT /customers/{id} -> Updates existing customer with new information. Requires Id.
- GET /customers/countries -> List each country and the total customers from that country (Desc)
- GET /customers/spending/highest -> Lists customers' spending. (Desc)
- GET /customers/{id}/popular/genre -> Returns the most popular genre that a customer has purchased tracks of. Gets mulitple genres if same value. 

Note: ErrorController also exists. All endpoints that do not match the aforementioned endpoints lead to a NotFound() response which is a part of the ErrorController.

## Build/Run the Application
- The user must change the SQLServer name. This can be done by changing the `stringBuilder.DataSource` in the `/Repositories/ConnectionHelper.cs` to their local Microsoft SQLServer.
- If using terminal, 
    - Use `dotnet run` to run once.
    - Use `dotnet watch run` if you want to re-run on save
    - Use `donet build` to just build the project
- If using Visual Studio
    - Press the IIS Express Button to run in debugging mode.
    - Press `Ctrl + F5` to run without debug mode. 



Note: Postman Collection is present inside the solution. 