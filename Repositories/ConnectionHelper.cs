﻿using System.Data.SqlClient;

namespace MyTunes.Repositories
{
    public class ConnectionHelper
    {
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder stringBuilder = new SqlConnectionStringBuilder();
            stringBuilder.DataSource = "PC7261\\SQLEXPRESS";
            stringBuilder.InitialCatalog = "Chinook";
            stringBuilder.IntegratedSecurity = true;
            return stringBuilder.ConnectionString;
        }
    }
}