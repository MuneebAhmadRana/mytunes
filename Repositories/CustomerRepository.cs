﻿using MyTunes.Models;
using MyTunes.Models.Domain;
using MyTunes.Models.DTOs;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;

namespace MyTunes.Repositories
{
    public class CustomerRepository
    {
        /// <summary>
        /// Retrieves all customers from the database
        /// </summary>
        /// <returns><see cref="List" /> of type <see cref="CustomerDTO"/></returns>
        public List<CustomerDTO> GetAllCustomers()
        {
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email from Customer";
            List<CustomerDTO> allCustomers = new List<CustomerDTO>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerDTO newCustomer = new CustomerDTO();
                                newCustomer.CustomerId = reader.GetInt32(0);
                                newCustomer.FirstName = reader.GetString(1);
                                newCustomer.LastName = reader.GetString(2);
                                newCustomer.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4))
                                {
                                    newCustomer.PostalCode = (string)reader[4];
                                }
                                if (!reader.IsDBNull(5))
                                {
                                    newCustomer.Phone = reader.GetString(5);
                                }
                                newCustomer.Email = reader.GetString(6);
                                allCustomers.Add(newCustomer);
                            }
                        }
                    }
                }

                return allCustomers;
            }
            catch (SqlException e)
            {
                throw new IOException(e.Message);
            }
        }

        /// <summary>
        /// Inserts a new customer to the database
        /// </summary>
        /// <param name="newCustomer">Customer to be inserted</param>
        /// <returns><see cref="CustomerDTO" /></returns>
        public bool CreateCustomer(CustomerDTO newCustomer)
        {
            bool success;
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) VALUES (@FirstName, @LastName,  @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", newCustomer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", newCustomer.LastName);
                        cmd.Parameters.AddWithValue("@Country", newCustomer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", newCustomer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", newCustomer.Phone);
                        cmd.Parameters.AddWithValue("@Email", newCustomer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
                return success;
            }
            catch (SqlException e)
            {
                throw new IOException(e.Message);
            }
        }

        /// <summary>
        /// Update existing customer based on provided details
        /// </summary>
        /// <param name="newCustomer"></param>
        /// <returns>Returns updated <see cref="CustomerDTO" /> object</returns>
        public CustomerDTO UpdateExistingCustomer(CustomerDTO newCustomer)
        {
            try
            {
                string sql = "UPDATE Customer SET FirstName=@FirstName, LastName=@LastName, Country=@Country, PostalCode=@PostalCode, Phone=@Phone, Email=@Email WHERE CustomerId = @CustomerId";
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", newCustomer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", newCustomer.LastName);
                        cmd.Parameters.AddWithValue("@Country", newCustomer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", newCustomer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", newCustomer.Phone);
                        cmd.Parameters.AddWithValue("@Email", newCustomer.Email);
                        cmd.Parameters.AddWithValue("@CustomerId", newCustomer.CustomerId);
                        cmd.ExecuteNonQuery();
                    }
                }

                return newCustomer;
            }
            catch (SqlException e)
            {
                throw new IOException(e.Message);
            }
        }

        /// <summary>
        /// Shows the number of customers per country
        /// </summary>
        /// <returns>Returns <see cref="CountryCount"/> object</returns>
        public List<CountryCount> GetCustomersPerCountry()
        {
            try
            {
                string sql = "SELECT Country, COUNT(Country) as Customers from Customer group by Country order by Count(Country) desc";
                List<CountryCount> countryList = new List<CountryCount>();
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CountryCount cc = new CountryCount
                                {
                                    Country = reader.GetString(0),
                                    Count = reader.GetInt32(1)
                                };
                                countryList.Add(cc);
                            }
                        }
                    }
                }
                return countryList;
            }
            catch (SqlException e)
            {
                throw new IOException(e.Message);
            }
        }

        /// <summary>
        /// Shows list of all customers and their total spendings in descending order
        /// </summary>
        /// <returns>list of <see cref="HighestSpender"/></returns>
        public List<HighestSpender> HighestSpenders()
        {
            try
            {
                string sql = "SELECT FirstName, LastName, Country, PostalCode, Phone, Email, Customer.CustomerId, Total from Invoice, Customer where Customer.CustomerId=Invoice.CustomerId order by Total desc";
                List<HighestSpender> hsList = new List<HighestSpender>();
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerDTO customer = new CustomerDTO();
                                customer.FirstName = reader.GetString(0);
                                customer.LastName = reader.GetString(1);
                                customer.Country = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                {
                                    customer.PostalCode = reader.GetString(3);
                                }
                                if (!reader.IsDBNull(4))
                                {
                                    customer.Phone = reader.GetString(4);
                                }
                                customer.Email = reader.GetString(5);
                                customer.CustomerId = reader.GetInt32(6);

                                HighestSpender hs = new HighestSpender();
                                hs.Total = reader.GetDecimal(7);
                                hs.Customer = customer;
                                hsList.Add(hs);
                            }
                        }
                    }
                }
                return hsList;
            }
            catch (SqlException e)
            {
                throw new IOException(e.Message);
            }
        }

        /// <summary>
        /// Gets the most popular genre(s) for a customer.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns <see cref="TopGenre"/></returns>
        public List<TopGenre> TopGenreForCustomer(int id)
        {
            try
            {
                string sql = "SELECT Genre.Name, COUNT(Genre.Name) " +
                            "FROM Customer " +
                            "INNER JOIN Invoice on Invoice.CustomerId = @CustomerId " +
                            "INNER JOIN InvoiceLine on InvoiceLine.InvoiceId = Invoice.InvoiceId, " +
                            "Genre, " +
                            "Track " +
                            "WHERE Invoice.CustomerId = Customer.CustomerId " +
                            "AND InvoiceLine.TrackId = Track.TrackId " +
                            "AND Track.GenreId = Genre.GenreId " +
                            "GROUP BY Genre.Name " +
                            "ORDER BY COUNT(Genre.Name) desc";
                List<TopGenre> tgList = new List<TopGenre>();
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                TopGenre tg = new TopGenre();
                                tg.Genre = reader.GetString(0);
                                tg.Count = reader.GetInt32(1);
                                tgList.Add(tg);
                            }
                        }
                    }
                }
                return tgList;
            }
            catch (SqlException e)
            {
                throw new IOException(e.Message);
            }
        }

        /// <summary>
        /// Fetches the specified customer from the database based on the provided Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns <see cref="CustomerDTO"/></returns>
        public CustomerDTO GetCustomerById(int id)
        {
            try
            {
                string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer Where CustomerId = @CustomerId";
                CustomerDTO customer = new CustomerDTO();
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.GetString(4);
                                customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
                return customer;
            }
            catch (SqlException e)
            {
                throw new IOException(e.Message);
            }
        }

        /// <summary>
        /// Getting Customer from the database based on the provided email. (Only one user per email)
        /// </summary>
        /// <param name="email">string format email</param>
        /// <returns>Returns <see cref="CustomerDTO"/> Object</returns>
        public CustomerDTO GetCustomerByEmail(string email)

        {
            try
            {
                string sql = "Select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email From Customer Where Email = @Email";
                CustomerDTO customer = new CustomerDTO();
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@Email", email);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.GetString(4);
                                customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
                return customer;
            }
            catch (SqlException e)
            {
                throw new IOException(e.Message);
            }
        }
    }
}