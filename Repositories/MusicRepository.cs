﻿using MyTunes.Models;
using MyTunes.Models.Domain;
using System.Data.SqlClient;
using System.IO;

namespace MyTunes.Repositories
{
    //Business Logic for Music Repository
    public class MusicRepository
    {
        /// <summary>
        /// Returns 5 random Tracks, Genres and Albums with their respective information.
        /// </summary>
        /// <returns>
        /// Object "Random5" which contains lists for Tracks, Genres and Albums
        /// </returns>
        public Random5 GetRandom()
        {
            string sqlArtist = "SELECT TOP 5 * FROM Artist ORDER BY NEWID()";
            string sqlGenre = "SELECT TOP 5 * FROM Genre ORDER BY NEWID()";
            string sqlTrack = "SELECT TOP 5 * FROM Track ORDER BY NEWID()";
            Random5 rand = new Random5();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    //Opening database connection
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sqlArtist, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Artist tempArtist = new Artist();
                                tempArtist.ArtistId = reader.GetInt32(0);
                                tempArtist.Name = reader.GetString(1);
                                rand.ArtistList.Add(tempArtist);
                            }
                        }
                    }
                    using (SqlCommand cmd = new SqlCommand(sqlTrack, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Track tempTrack = new Track();
                                tempTrack.TrackId = reader.GetInt32(0);
                                tempTrack.Name = reader.GetString(1);
                                tempTrack.AlbumId = reader.GetInt32(2);
                                tempTrack.MediTypeId = reader.GetInt32(3);
                                tempTrack.GenreId = reader.GetInt32(4);
                                tempTrack.Milliseconds = reader.GetInt32(6);
                                tempTrack.Bytes = reader.GetInt32(7);
                                tempTrack.UnitPrice = reader.GetDecimal(8);
                                rand.TrackList.Add(tempTrack);
                            }
                        }
                    }
                    using (SqlCommand cmd = new SqlCommand(sqlGenre, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Genre tempGenre = new Genre();
                                tempGenre.GenreId = reader.GetInt32(0);
                                tempGenre.Name = reader.GetString(1);
                                rand.GenreList.Add(tempGenre);
                            }
                        }
                    }
                }
                return rand;
            }
            catch (SqlException e)
            {
                throw new IOException(e.Message);
            }
        }

        /// <summary>
        /// Returns track name, album, genre and artist name for a searched track
        /// </summary>
        /// <param name="trackName"></param>
        /// <returns>
        /// Returns an object TrackInfo
        /// </returns>
        public TrackInfo GetTrackInfo(string trackName)
        {
            string sql = "SELECT Track.Name as N'Track Name', Artist.Name as N'Artist Name', Album.Title as N'Album Title', Genre.Name as N'Genre' " +
                            "FROM Artist, Track, Album, Genre " +
                            "WHERE Track.GenreId = Genre.GenreId " +
                            "AND Track.Name = @TrackName " +
                            "AND Track.AlbumId = Album.AlbumId " +
                            "AND Artist.ArtistId = Album.ArtistId";
            TrackInfo track = new TrackInfo();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand trackCommand = new SqlCommand(sql, conn))
                    {
                        trackCommand.Parameters.AddWithValue("@TrackName", trackName);
                        using (SqlDataReader reader = trackCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                track.TrackName = reader.GetString(0);
                                track.ArtistName = reader.GetString(1);
                                track.AlbumTitle = reader.GetString(2);
                                track.GenreName = reader.GetString(3);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                throw new IOException(e.Message);
            }
            return track;
        }
    }
}