# Queries for CRUD

## Music Controller
1. 5 Random Artist
    -   **SELECT TOP 5 Artist.Name FROM Artist
        ORDER BY NEWID()**
    -   **SELECT TOP 5 Genre.Name FROM Genre
        ORDER BY NEWID()**
    -   **SELECT TOP 5 Track.Name FROM Track
        ORDER BY NEWID()**
2. Search Track
    -   **SELECT Track.Name as N'Track Name', Artist.Name as N'Artist Name', Album.Title as N'Album Title', Genre.Name as N'Genre'
        FROM Artist, Track, Album, Genre
        WHERE Track.GenreId = Genre.GenreId
        AND Track.Name = N'Fast As a Shark'
        AND Track.AlbumId = Album.AlbumId
        AND Artist.ArtistId = Album.ArtistId**


## Customers Controller
1.  Read all the customers in the database.
    -   **SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email from Customer**
2.  Add a new customer to the database.
    -   **INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) VALUES (N'FirstName', N'LastName', N'country', 1234, 123123123, N'mail@mail.com')**
3.  Update an existing customer
    -   **UPDATE Customer SET FirstName=N'FirstName', LastName=N'LastName', Country=N'Country', PostalCode=1234, Phone=12398123, Email=N'email@mail.com' WHERE CustomerId = 60**
4.  Number of customers in each contry, orderer descending
    -   **SELECT Country, COUNT(Country) as Customers from Customer group by Country order by Count(Country) desc**
5.  Customers who are the highest spenders
    -   **SELECT FirstName, Total from Invoice, Customer where Customer.CustomerId=Invoice.CustomerId order by Total desc**
6.  For a give customer, their most popular genre
    -   **SELECT TOP 1 Genre.Name, COUNT(Genre.Name) as total 
        FROM Customer
        INNER JOIN Invoice on Invoice.CustomerId = 7
        INNER JOIN InvoiceLine on InvoiceLine.InvoiceId = Invoice.InvoiceId,
        Genre,
        Track
        WHERE Invoice.CustomerId = Customer.CustomerId
        AND InvoiceLine.TrackId = Track.TrackId
        AND Track.GenreId = Genre.GenreId
        GROUP BY Genre.Name
        ORDER BY COUNT(Genre.Name) desc**
    
