using Microsoft.AspNetCore.Mvc;
using MyTunes.Models;
using MyTunes.Models.Domain;
using MyTunes.Repositories;

namespace MyTunes.Controllers
{
    [ApiController]
    [Route("/api/v1/[controller]")]
    public class MusicController : ControllerBase
    {
        private readonly MusicRepository _musicRepository;

        public MusicController(MusicRepository musicRepository)
        {
            _musicRepository = musicRepository;
        }

        [HttpGet]
        public ActionResult<Random5> Home()
        {
            return Ok(_musicRepository.GetRandom());
        }

        [HttpGet("search")]
        public ActionResult<TrackInfo> Search(string track)
        {
            return Ok(_musicRepository.GetTrackInfo(track));
        }
    }
}