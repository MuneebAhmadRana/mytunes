using Microsoft.AspNetCore.Mvc;
using MyTunes.Models;
using MyTunes.Models.Domain;
using MyTunes.Models.DTOs;
using MyTunes.Repositories;
using System.Collections.Generic;

namespace MyTunes.Controllers
{
    [ApiController]
    [Route("/api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly CustomerRepository _customerRepository;

        public CustomersController(CustomerRepository repos)
        {
            _customerRepository = repos;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CustomerDTO>> Home()
        {
            IEnumerable<CustomerDTO> customers = _customerRepository.GetAllCustomers();
            return Ok(customers);
        }

        /// <summary>
        /// Endpoint for retrieving a customer from the database, based on Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult<CustomerDTO> GetCustomerById(int id)
        {
            return Ok(_customerRepository.GetCustomerById(id));
        }

        /// <summary>
        /// Endpoint for adding customer to database.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<CustomerDTO> PostCustomer(CustomerDTO customer)
        {
            CustomerDTO c;
            //Creating customer
            bool success = _customerRepository.CreateCustomer(customer);
            //Getting customer by email
            c = _customerRepository.GetCustomerByEmail(customer.Email);
            //Create a location link in response header which leads to the created customer
            return CreatedAtAction(nameof(GetCustomerById), new { id = c.CustomerId }, success);
        }

        [HttpPut("{id}")]
        public ActionResult<string> PutCustomer(int id, CustomerDTO customer)
        {
            // If provided id does not match the customers id, its a bad request
            if (id != customer.CustomerId)
            {
                return BadRequest();
            }
            _customerRepository.UpdateExistingCustomer(customer);
            //Redirects to api/v1/customers/id endpoint, and returns the updated customer.
            return RedirectToAction(nameof(GetCustomerById), new { id = customer.CustomerId });
        }

        [HttpGet("countries")]
        public ActionResult<CountryCount> TopCountries()
        {
            return Ok(_customerRepository.GetCustomersPerCountry());
        }

        [HttpGet("spending/highest")]
        public ActionResult<HighestSpender> HighestSpenders()
        {
            return Ok(_customerRepository.HighestSpenders());
        }

        [HttpGet("{id}/popular/genre")]
        public ActionResult<TopGenre> PopularGenreById(int id)
        {
            List<TopGenre> tgList = _customerRepository.TopGenreForCustomer(id);
            List<TopGenre> returnList = new List<TopGenre>();
            int topGenreCount = tgList[0].Count;
            int index = 0;
            while (tgList[index].Count == topGenreCount)
            {
                returnList.Add(tgList[index]);
                index++;
            }
            return Ok(returnList);
        }
    }
}