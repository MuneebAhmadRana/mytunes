﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Controllers
{
    [ApiController]
    [Route("{*url}", Order = 999)]
    public class ErrorController : ControllerBase
    {
        [HttpGet]
        public ActionResult Index()
        {
            return NotFound();
        }
    }
}
